#!/usr/bin/python3
import os
import sys

from ansible.plugins.inventory import BaseInventoryPlugin, Constructable, Cacheable

p = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.insert(0, p)
from data import serverlist

class InventoryModule(BaseInventoryPlugin, Constructable, Cacheable):

    NAME = 'entrouvert'  # used internally by Ansible, it should match the file name but not required

    def verify_file(self, path):
        return True

    def parse(self, inventory, loader, path, cache=False):

        #self.loader = loader
        #self.inventory = inventory
        #self.templar = Templar(loader=loader)

        # call base method to ensure properties are available for use with other helper methods
        super(InventoryModule, self).parse(inventory, loader, path, cache)


        """
        # this method will parse 'common format' inventory sources and
        # update any options declared in DOCUMENTATION as needed
        config = self._read_config_data(path)

        # if NOT using _read_config_data you should call set_options directly,
        # to process any defined configuration for this plugin,
        # if you don't define any options you can skip
        #self.set_options()

        # example consuming options from inventory source
        mysession = apilib.session(user=self.get_option('api_user'),
                                    password=self.get_option('api_pass'),
                                    server=self.get_option('api_server')
        )
        """


        """
        # make requests to get data to feed into inventory
        mydata = mysession.getitall()

        #parse data and create inventory objects:
        for colo in mydata:
            for server in mydata[colo]['servers']:
                self.inventory.add_host(server['name'])
                self.inventory.set_variable(server['name'], 'ansible_host', server['external_ip'])
        """

        for s in serverlist:
            self.inventory.add_host(s)
