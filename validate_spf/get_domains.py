from hobo.environment.models import Hobo, Variable
import json

domains = {}

for hobo in Hobo.objects.all():
    try:
        address = Variable.objects.get(name='default_from_email')
    except Variable.DoesNotExist:
        continue
    if address and address.value != '':
        domain = address.value.split('@')[-1]
        domains.setdefault(domain, [])
        domains[domain].append((address.value, hobo.slug))

if domains:
    print(json.dumps(domains))
