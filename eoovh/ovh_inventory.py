#!/usr/bin/env python3

import ovh
import socket
import yaml

client = ovh.Client()

ovh_servers = {}
servers = {k: {} for k in client.get('/dedicated/server')}
for s, _ in servers.items():
    name, _, addresslist = socket.gethostbyaddr(socket.gethostbyname(s))
    servers[s]['name'] = name
    servers[s]['ovh_name'] = s
    servers[s]['address'] = addresslist[0]
    servers[s]['base'] = client.get('/dedicated/server/%s' % s)
    servers[s]['ips'] = client.get('/dedicated/server/%s/ips' % s)
    servers[s]['options'] = client.get('/dedicated/server/%s/option' % s)
    servers[s]['hardware'] = client.get('/dedicated/server/%s/specifications/hardware' % s)
    servers[s]['ip'] = client.get('/dedicated/server/%s/specifications/ip' % s)
    servers[s]['network'] = client.get('/dedicated/server/%s/specifications/network' % s)
    servers[s]['interfaces'] = {i: {} for i in client.get('/dedicated/server/%s/networkInterfaceController' % s)}
    for m, _ in servers[s]['interfaces'].items():
        servers[s]['interfaces'][m]['type'] = client.get('/dedicated/server/%s/networkInterfaceController/%s' % (s, m))
        if servers[s]['interfaces'][m]['type']['linkType'] == 'public':
            if 'macaddress' in servers[s].keys():
                print('ERROR: %s has multiple public network interfaces' % s)
            servers[s]['macaddress'] = m

    if servers[s]['name'] != servers[s]['base']['reverse'] or '%s.' % servers[s]['name'] != servers[s]['base']['reverse']:
        print('ERROR: %s reverse DNS is wrong' % s)
    if servers[s]['address'] != servers[s]['base']['ip'] or servers[s]['address'] != servers[s]['network']['routing']['ipv4']['ip']:
        print('ERROR: %s ip address is wrong' % s)

    # reindex according to entrouvert key
    ovh_servers[servers[s]['name']] = servers[s]

print(yaml.dump(ovh_servers))
