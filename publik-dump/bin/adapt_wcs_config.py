#!/usr/bin/python2
import argparse
import pickle

parser = argparse.ArgumentParser()
parser.add_argument("file")
parser.add_argument("--host")
parser.add_argument("--password")
args = parser.parse_args()

with open(args.file) as fh:
    data = pickle.loads(fh.read())

data["postgresql"]["host"] = args.host
data["postgresql"]["password"] = args.password
data["postgresql"]["createdb-connection-params"]["host"] = args.host
data["postgresql"]["createdb-connection-params"]["password"] = args.password
print(data["postgresql"])

with open(args.file, "w") as fh:
    fh.write(pickle.dumps(data))
