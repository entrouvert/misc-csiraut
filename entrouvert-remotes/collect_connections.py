# hobo - portal to configure and deploy applications
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import connection

from hobo.multitenant.middleware import TenantMiddleware


class Command(BaseCommand):

    def handle(self, verbosity=0, *args, **options):
        entries = {}
        for tenant in TenantMiddleware.get_tenants():
            if verbosity > 0:
                print(u'* Running command on tenant %s' % tenant.domain_url)
            connection.set_tenant(tenant)
            key = tenant.domain_url
            entries[key] = []
            for entry in self.tenant_entries():
                entries[key].append(entry)

        # hack (see TODO further on)
        if os.path.isfile('/usr/bin/passerelle-manage'):
            entries['wcs'] = []
            for entry in self.wcs_remotes():
                entries['wcs'].append(entry)

        print(json.dumps(entries))

    def tenant_entries(self, *args, **options):
        entries = []
        if os.path.isfile('/usr/bin/passerelle-manage'):
            entries.extend(self.passerelle_remotes())
        ## TODO: proper wcs entries collection
        # if os.path.isfile('/usr/bin/wcs-manage'):
        #    entries.extend(self.wcs_remotes())
        if os.path.isfile('/usr/bin/authentic2-multitenant-manage'):
            entries.extend(self.authentic_remotes())
        return entries

    def authentic_remotes(self):
        for s in settings.LDAP_AUTH_SETTINGS:
            if isinstance(s['url'], list):
                for entry in s['url']:
                    yield entry
            else:
                yield(s['url'])

    def passerelle_remotes(self):
        from passerelle.views import get_all_apps

        for app in get_all_apps():
            for connector in app.objects.all():
                for attr in connector.__dict__.keys():
                    if attr.endswith('url'):
                        url = getattr(connector, attr, None)
                        if url:
                            yield(url)

    def wcs_remotes(self):
        """
        os.system('grep --color https:// */{formdefs,workflows,datasources,wscalls}/* | grep -v href= | grep -v src= | grep -o "https://.*" | sort | uniq')
        """

        ls = '''https://api.wavebricks.com/#/register?firstname=[form_var_firstname]&lastname=[form_var_lastname]&email=[form_var_courriel]&type_utilisateur=@IF&referrer=EMS2017
        https://sp01.vincennes.fr/webhook-comptecitoyen/
        https://bacasable.atreal.fr/~demowsdia/openads/services/rest_entry.php/dia/
        https://secure2.grandnancy.eu/grcnancywebservices/symfony/web/app_dev.php/eo/test
        https://si.metzmetropole.fr/index.php
        https://si.metzmetropole.fr/index.php?appid=252&q=gare
        https://si-ws-dev.metzmetropole.fr/adp/putAnomaliePublik.php
        https://si-ws.metzmetropole.fr/adp/____putAnomaliePublik.php
        https://si-ws.metzmetropole.fr/adp/putAnomaliePublik.php
        https://si-ws.metzmetropole.fr/sig/getquartierbyadress.php?q=3%20rue%20des%20eglantiers
        https://srv-geoserver.mairie-metz.fr/public/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=public:fdp_voi_nume&maxFeatures=50&outputFormat=application/json
        https://srv-siapps.mairie-metz.fr/index.php'''

        for l in ls.splitlines():
            yield l.strip()


command = Command()
command.handle()
