#!/usr/bin/env python3
import os
import sys
import time
from selenium import webdriver
from bs4 import BeautifulSoup

exec(open(os.path.expanduser("~/.config/sympa/sympa.py")).read())
base = 'https://listes.entrouvert.com'


def get_browser():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--incognito')
    browser = webdriver.Chrome(options=options)
    browser.get(base)
    us = browser.find_element_by_id("email_login")
    pa = browser.find_element_by_id("passwd")
    us.send_keys(username)
    pa.send_keys(password)
    el = browser.find_element_by_name("action_login")
    el = browser.find_element_by_id("login-dropdown")
    webdriver.ActionChains(browser).move_to_element(el).pause(1).click(el).perform()
    browser.find_element_by_name("action_login").click()
    return browser


def list_lists(browser):
    soup = BeautifulSoup(browser.page_source, features="lxml")
    lists = ['%s%s' % (base, li.find('a')['href']) for li in soup.find_all('li', class_='listenum')]
    return lists


def get_dkim_settings(browser, list_url):
    settings = {'dkim_feature': True,
                'dkim_signature': True,
                'dmarc': True}
    url = list_url.replace('/info/', '/edit_list_request/') + '/dkim'
    browser.get(url)
    select = browser.find_element_by_id('single_param.dkim_feature.name')
    options = select.find_elements_by_tag_name('option')
    for option in options:
        if option.is_selected():
            if option.get_attribute('value') == "off":
                settings['dkim_feature'] = False

    select = browser.find_element_by_name('multiple_param.dkim_signature_apply_on')
    options = select.find_elements_by_tag_name('option')
    sel_options = 0
    for option in options:
        if option.is_selected() and option.get_attribute('value') != 'none':
            sel_options += 1
    if not sel_options:
        settings['dkim_signature'] = False

    select = browser.find_element_by_name('multiple_param.dmarc_protection.mode')
    options = select.find_elements_by_tag_name('option')
    sel_options = 0
    for option in options:
        if option.is_selected() and option.get_attribute('value') != 'none':
            sel_options += 1
    if not sel_options:
        settings['dmarc'] = False
    return settings


def disable_dkim(browser, list_url):
    url = list_url.replace('/info/', '/edit_list_request/') + '/dkim'
    browser.get(url)
    select = browser.find_element_by_id('single_param.dkim_feature.name')
    options = select.find_elements_by_tag_name('option')
    options[0].click()

    select = browser.find_element_by_name('multiple_param.dkim_signature_apply_on')
    options = select.find_elements_by_tag_name('option')
    for option in options:
        if option.is_selected() and option.get_attribute('value') != 'none':
            option.click()
        if not option.is_selected() and option.get_attribute('value') == "none":
            option.click()
    select = browser.find_element_by_name('multiple_param.dmarc_protection.mode')
    options = select.find_elements_by_tag_name('option')
    for option in options:
        if option.is_selected() and option.get_attribute('value') != 'none':
            option.click()
        if not option.is_selected() and option.get_attribute('value') == "none":
            option.click()
    update = browser.find_element_by_name('action_edit_list')
    update.click()


def set_dmarc_phrase(browser, list_url):
    url = list_url.replace('/info/', '/edit_list_request/') + '/dkim'
    browser.get(url)
    select = browser.find_element_by_id('single_param.dmarc_protection.phrase.name')
    options = select.find_elements_by_tag_name('option')
    for option in options:
        if option.get_attribute('value') == 'name_and_email':
            option.click()
    update = browser.find_element_by_name('action_edit_list')
    update.click()


if __name__ == '__main__':
    browser = get_browser()
    list_urls = list_lists(browser)
    if sys.argv[1] == 'show':
        for list_url in list_urls:
            print(list_url, get_dkim_settings(browser, list_url))
    elif sys.argv[1] == 'set_dmarc_phrase':
        for list_url in list_urls:
            set_dmarc_phrase(browser, list_url)
        time.sleep(1)
