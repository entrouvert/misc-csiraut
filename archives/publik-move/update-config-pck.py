#!/usr/bin/python2
import pickle
import sys

wcs_password = open('/srv/nfs/etc/wcs/password').read().strip()

def adapt_wcs(path, dbname, dbmaster='prod.saas.entrouvert.org.clusters.entrouvert.org'):
    dbname = dbname.strip()
    with open(path, 'rb') as fh:
        cf = pickle.load(fh)
        if not 'postgresql' in cf:
            print('%s not a postgresql instance' % path)
        cf['postgresql']['host'] = dbmaster
        cf['postgresql']['port'] = '5432'
        cf['postgresql']['user'] = 'wcs'
        cf['postgresql']['password'] = wcs_password
        cf['postgresql']['database'] = dbname

    with open(path, 'wb') as fh:
        pickle.dump(cf, fh)

adapt_wcs(sys.argv[1], sys.argv[2])
