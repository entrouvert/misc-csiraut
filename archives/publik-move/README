[converted from https://dev.entrouvert.org/projects/sysadmin/wiki/D%C3%A9placement_Publik_Mutualise with html2text]

****** Déplacement Publik Mutualisé¶ ******
    * Déplacement_Publik_Mutualisé
          o Procédure
                # Avant
                # Pendant

Scripts de déploiement: git://git.entrouvert.org:publik-move.git
    * list-services.py doit être sur le vieux hobo

Utilisation du script :
    * publik-cluster-copy update : interroge le vieux hobo et stocke la liste
      des tenants et des services
    * publik-cluster-copy sync : copie les fichiers et les dumps des bases de
      données dans /srv/ceph/migration
    * publik-cluster-copy ls : liste les tenants
    * publik-cluster-copy take --tenant demarches-
      chateauroux.test.entrouvert.org : copie, adapte et pose les fichiers et
      les db
    * le flag --verbose permet de suivre les commandes exécutées

Pour une instance au-quotidien, procédure identique, nécessite le flag --auqo
et --tenant :
    * ./publik-cluster-copy sync --auquo --tenant demo.test.au-quotidien.com --
      verbose
    * ./publik-cluster-copy take --auquo --tenant demo.test.au-quotidien.com --
      wcsdbname wcs_demo_test_au_quotidien_com --verbose

***** Procédure¶ *****
**** Avant¶ ****
   1. Pré-synchroniser tous les tenants: ./publik-cluster-copy sync
   2. parcourir et dupliquer /etc/app/settings.d/ /etc/cron.hourly etc.
         * ssh app.test.entrouvert.org ls -l /etc/cron.hourly /etc/
           cron.daily /etc/cron.d
         * ssh app.test.entrouvert.org ls -l /etc/app/settings.d
   1. Préparer les prochains fichiers de zone DNS
         * à coups de :537,547 s/E[ \t]*.*/E tst/ ou de :537,547 s/E
           [ \t]*.*/E prod/
         * ne pas oublier les sous-domaines de au-quotidien.com!
   1. Vérifier si les certificats sont dispos sur le nouveau serveur
   2. Anticiper des dossiers posés sur des partitions différents (e.g. dossier
      uploads symlink vers /var/lib/wcs-au-quotidien-uploads/)
   3. Si nécessaire convertir wcs vers postgresql: CREATE DATABASE
      wcs_demo_test_au_quotidien_com WITH OWNER="wcs" TEMPLATE=template0
      LC_COLLATE='fr_FR.utf8' LC_CTYPE='fr_FR.utf8'; ensuite sudo -u wcs wcs-
      manage convert_to_sql -d demo.test.au-quotidien.com --database
      wcs_demo_test_au_quotidien_com

**** Pendant¶ ****
   1. couper les cron wcs sur l'ancien et le nouveau:
      ssh wcs.test.entrouvert.org sudo sed 's/^\*/##\*/' -i /etc/cron.d/wcs
      ssh wcs.node1.test.saas.entrouvert.org sudo sed 's/^\*/##\*/' -i /etc/cron.d/wcs
      # ou
      ssh wcs.entrouvert.org sudo sed 's/^\*/##\*/' -i /etc/cron.d/wcs
      ssh wcs.node1.prod.saas.entrouvert.org sudo sed 's/^\*/##\*/' -i /etc/cron.d/wcs
          o Vérifier que cron ne n'exécute plus avant de continuer : ssh wcs.
            {test|prod}.entrouvert.org ps waux | grep wcs-manage
   2. couper les vhost nginx: script qui déploie des 503 là ou c'est
      nécessaire:
          o sur gallinette/chicon, mettre à jour: /root/000_503-service-
            unavailable
          o lancer /root/diffuse-nginx-config
   3. sur le nouvel hyperviseur (e.g. node1.test.saas.entrouvert.org)
      $ sudo su -
      # cd /root/publik-move
      # git pull
      # ./publik-cluster-copy sync --tenant hobo-foobar.tst.entrouvert.org # prend du temps
      # ./publik-cluster-copy take --tenant hobo-foobar.tst.entrouvert.org
   4. invalider le client sur gallinette/chicon: tenant => tenant-migrated
          o ssh gallinette.entrouvert.org sudo /root/invalide-tenants-en-503
   5. dans le cas de tenant avec dossier uploads symlink vers /var/lib/wcs-au-
      quotidien-uploads/<site>, synchroniser ce dossier à la main
        upl=demarches.e-service.seine-et-marne.fr
        test -L /srv/ceph/lib/wcs/${upl}/uploads && echo "HAAAAA COPY"
        rm /srv/ceph/lib/wcs/${upl}/uploads
        rsync -apv --info=progress2 auquo.entrouvert.org:/var/lib/wcs-au-quotidien-
        uploads/$upl /srv/ceph/migration/wcs-au-quotidien-uploads/
        mv /srv/ceph/migration/wcs-au-quotidien-uploads/${upl}/uploads /srv/ceph/lib/wcs/${upl}/
   6. substituer le fichier de zone DNS, rndc reload
   7. attendre 2 minutes (ttl DNS) et tester le plus possible
   8. relancer les crons wcs sur l'ancien et le nouveau:
      ssh wcs.test.entrouvert.org sudo sed 's/^##\\*/\*/' -i /etc/cron.d/wcs
      ssh wcs.node1.test.saas.entrouvert.org sudo sed 's/^##\\*/\*/' -i /etc/cron.d/wcs
      # ou
      ssh wcs.entrouvert.org sudo sed 's/^##\\*/\*/' -i /etc/cron.d/wcs
      ssh wcs.node1.prod.saas.entrouvert.org sudo sed 's/^##\\*/\*/' -i /etc/cron.d/wcs
