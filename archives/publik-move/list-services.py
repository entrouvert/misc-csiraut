import urlparse
from hobo.environment.models import AVAILABLE_SERVICES
from hobo.multitenant.middleware import TenantMiddleware
from django.db import connection

bases_wcs = {
'calvados.test.entrouvert.org': 'wcs_calvados_test_entrouvert_org',
'cdg59.test.au-quotidien.com': '',
'cud.au-quotidien.com': '',
'demarches-alfortville.test.entrouvert.org': 'wcs-alfortville',
'demarches-amiens.test.entrouvert.org': 'wcs_demarches_amiens_test_entrouvert_org',
'demarches-arles.test.entrouvert.org': 'wcs_demarches_arles_test_entrouvert_org',
'demarches-atreal.test.entrouvert.org': 'wcs_demarches_atreal_test_entrouvert_org',
'demarches-auch.test.entrouvert.org': 'wcs_demarches_auch_test_entrouvert_org',
'demarches-blois.test.entrouvert.org': 'wcs_demarches_blois_test_entrouvert_org',
'demarches-cd22.test.entrouvert.org': 'wcs_cd22',
'demarches-cd44.test.entrouvert.org': 'wcs_demarches_cd44_test_entrouvert_org',
'demarches-chateauroux.test.entrouvert.org': 'wcs_demarches_chateauroux_test_entrouvert_org',
'demarches-clisson.test.entrouvert.org': 'wcs_demarches_clisson_test_entrouvert_org',
'demarches-cnil.test.entrouvert.org': 'wcs_demarches_cnil_test_entrouvert_org',
'demarches-csma.test.entrouvert.org': 'wcs_demarches_csma_test_entrouvert_org',
'demarches-departement06.test.entrouvert.org': 'wcs_departement06',
'demarches-dreux.test.entrouvert.org': 'wcs_demarches_dreux_test_entrouvert_org',
'demarches-e-service-seine-et-marne.test.entrouvert.org': 'wcs_demarches_e_service_seine_et_marne_test_entrouvert_org',
'demarches-fsb.test.au-quotidien.com': 'wcs_demarches_fsb_test_au_quotidien_com',
'demarches-gorges.test.entrouvert.org': 'wcs_demarches_gorges_test_entrouvert_org',
'demarches-grenoble.test.entrouvert.org': 'wcs_demarches_grenoble_test_entrouvert_org',
'demarches-haute-goulaine.test.entrouvert.org': 'wcs_demarches_haute_goulaine_test_entrouvert_org',
'demarches-hautes-alpes.test.entrouvert.org': 'wcs_demarches_hautes_alpes_test_entrouvert_org',
'demarches-implicit.test.entrouvert.org': 'wcs_demarches_implicit_test_entrouvert_org',
'demarches-lareunion.test.entrouvert.org': 'wcs_demarches_lareunion_test_entrouvert_org',
'demarches-lenord.test.entrouvert.org': 'wcs_demarches_lenord_test_entrouvert_org',
'demarches-lenord.test.entrouvert.org.DELETED.invalid': 'wcs_demarches_lenord_test_entrouvert_org',
'demarches-lozere.test.entrouvert.org': 'wcs_demarches_lozere_test_entrouvert_org',
'demarches-matrik.test.entrouvert.org': 'wcs_demarches_matrik_test_entrouvert_org',
'demarches-mauguio-carnon.test.entrouvert.org': 'wcs_demarches_mauguio_carnon_test_entrouvert_org',
'demarches-metz.test.entrouvert.org': 'wcs_metz',
'demarches-meudon.test.entrouvert.org': 'wcs_demarches_meudon_test_entrouvert_org',
'demarches-meyzieu.test.au-quotidien.com': 'demarches_meyzieu_test_au_quotidien_com',
'demarches-montsac-sicoval.test.entrouvert.org': 'wcs_demarches_montsac_sicoval_test_entrouvert_org',
'demarches-nancy.test.entrouvert.org': 'wcs_demarches_nancy_test_entrouvert_org',
'demarches-nancy.test.entrouvert.org.invalid': 'wcs_demarches_nancy_test_entrouvert_org',
'demarches-orleans.test.entrouvert.org': 'demarches_orleans_test_entrouvert_org',
'demarches-planesou-sicoval.test.entrouvert.org': 'wcs_demarches_planesou_sicoval_test_entrouvert_org',
'demarches-publik-light.test.entrouvert.org': 'wcs_demarches_publik_light_test_entrouvert_org',
'demarches-quimper.test.entrouvert.org': 'wcs_demarches_quimper_test_entrouvert_org',
'demarches-rochefort.test.entrouvert.org.DELETED.invalid': 'wcs_demarches_rochefort_test_entrouvert_org',
'demarches-rouen.test.entrouvert.org': 'wcs_demarches_rouen_test_entrouvert_org',
'demarches-saint-chamond.test.entrouvert.org': 'wcs_venissieux',
'demarches-saint-lo.test.entrouvert.org': 'wcs_demarches_saint_lo_test_entrouvert_org',
'demarches-saone-et-loire.test.entrouvert.org': 'wcs_demarches_saone_et_loire_test_entrouvert_org',
'demarches-sicoval.test.entrouvert.org': 'wcs_demarches_sicoval_test_entrouvert_org',
'demarches-somme.test.entrouvert.org': 'wcs_demarches_somme_test_entrouvert_org',
'demarches-strasbourg.test.entrouvert.org': 'wcs_demarches_strasbourg_test_entrouvert_org',
'demarches-toulouse.test.entrouvert.org': 'wcs_demarches_toulouse_test_entrouvert_org',
'demarches-tours.test.entrouvert.org': 'wcs_demarches_tours_test_entrouvert_org',
'demarches-up.test.entrouvert.org': 'wcs_demarches_up_test_entrouvert_org',
'demarches-validation.test.entrouvert.org': 'wcs_demarches_validation_test_entrouvert_org',
'demarches-venissieux.test.entrouvert.org': 'wcs_venissieux',
'demarches-vincennes.test.au-quotidien.com': 'wcs_demarches_vincennes_test_au_quotidien_com',
'demarches2016.alfortville.fr': 'wcs_demarches2016_alfortville_fr',
'demo-calvados.test.au-quotidien.com': '',
'demo-cdg59.test.au-quotidien.com': '',
'demo.test.au-quotidien.com': '',
'e-megalis.test.au-quotidien.com': '',
'echirolles.dev.au-quotidien.com': '',
'eservice-cdg59.test.au-quotidien.com': '',
'eservices-cch.test.entrouvert.org': 'wcs_eservices_cch_test_entrouvert_org',
'fondettes.test.au-quotidien.com': 'wcs_fondettes_test_au_quotidien_com',
'fsb.test.au-quotidien.com.invalid': 'wcs_demarches_fsb_test_au_quotidien_com',
'graphviz.dev.au-quotidien.com': '',
'greville-hague.test.entrouvert.org': 'wcs_greville_hague_test_entrouvert_org',
'jerome.au-quotidien.com': '',
'jerome2.test.au-quotidien.com': '',
'jobourg.test.entrouvert.org': 'wcs_jobourg_test_entrouvert_org',
'koshie.test.au-quotidien.com': '',
'matrice.test.au-quotidien.com': '',
'meaux.test.au-quotidien.com': 'wcs-meaux',
'ng.dev.au-quotidien.com': '',
'omonville-la-rogue.test.entrouvert.org': 'wcs_omonville_la_rogue_test_entrouvert_org',
'orleans.test.au-quotidien.com': '',
'paiement.test.au-quotidien.com': '',
'poissy.test.au-quotidien.com': '',
'sitiv.test.au-quotidien.com': '',
'test.test.au-quotidien.com': '',
'tonneville.test.entrouvert.org': 'wcs_tonneville_test_entrouvert_org',
'urville-nacqueville.test.entrouvert.org': 'wcs_urville_nacqueville_test_entrouvert_org'
}
tenant = connection.get_tenant()
# print '%s hobo-balancer' % tenant.domain_url
# print 'hobo %s %s' % (tenant.domain_url, tenant.schema_name)

print "'%s': [" % tenant.domain_url,
print "{'service': 'hobo', 'hostname': '%s', 'schema': '%s'}," % (tenant.domain_url, tenant.schema_name),

for service in AVAILABLE_SERVICES:
    for site in service.objects.all():
        servicename = site.Extra.service_id
        hostname = urlparse.urlsplit(site.base_url).netloc.split(':')[0]
        if site.secondary:
            #print '# (secondary) %s %s-balancer' % (hostname, servicename)
            continue
        if servicename == 'hobo':
            #print '# (hobo) %s %s-balancer' % (hostname, servicename)
            continue
        schema = TenantMiddleware.hostname2schema(hostname)
        if servicename == 'wcs':
            schema = bases_wcs[hostname]
        print "{'service': '%s', 'hostname': '%s', 'schema': '%s'}," % (servicename, hostname, schema),
print '],'
